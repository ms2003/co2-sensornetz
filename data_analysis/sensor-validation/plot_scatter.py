#!/usr/bin/env python3

import sys

import matplotlib.pyplot as plt
import pandas as pd

SAMPLES_DIR = sys.argv[1]

_bme = pd.read_csv(f"{SAMPLES_DIR}co2_bme.csv", index_col="time")
_eee = pd.read_csv(f"{SAMPLES_DIR}co2_eee.csv", index_col="time")
_348 = pd.read_csv(f"{SAMPLES_DIR}co2_348.csv", index_col="time")

lines = [
    (_bme, "BME680", "co2.value"),
    (_eee, "CCS811 (0xeee)", "env.co2concentration"),
    (_348, "CCS811 (0x348)", "env.co2concentration"),
]

plt.figure(figsize=(16, 8))
plt.style.use("seaborn")
plt.ylabel("$CO_2$ concentration in ppm", fontsize=22)
plt.xlabel("Time in minutes", fontsize=22)
plt.xticks(size=20)
plt.yticks(size=20)

palette = plt.get_cmap("Set1")

if _bme.shape[0] == _eee.shape[0] == _348.shape[0]:
    minutes = [x for x in range(_bme.shape[0])]
else:
    sys.stderr.write("Samples do not have same amount of lines.\n")
    sys.exit(1)

i = 0
for entry in lines:
    df = entry[0]
    plt.plot(
        minutes,
        df[entry[2]],
        marker="",
        color=palette(i),
        linewidth=2,
        alpha=0.9,
        label=entry[1],
    )
    i += 1

plt.legend(
    loc="upper center", bbox_to_anchor=(0.5, 1.12), shadow=True, ncol=3, fontsize=20
)
plt.savefig("validation.png")
