#!/usr/bin/env python3

""" dwd-fetcher

Script to fetch weather data for a specific station from the german meterological
institute (DWD - Deutscher Wetterdiesnt) and write them to an lokal InfluxDB database.

See file LICENSE for license information
"""

import argparse
import calendar
import csv
import sys
import time
import urllib.parse
import urllib.request
from datetime import datetime
from pathlib import Path

import requests

# Quelle: Deutscher Wetterdienst
DWD_POI_URL = "https://opendata.dwd.de/weather/weather_reports/poi/"

TIME_FILE_PATH = "/tmp/dwd_last"


def get_file_from_url(url):
    try:
        response = urllib.request.urlopen(url)
        # print(
        #    "returned status: {} - file size: {}".format(
        #        response.status, response.getheader("Content-Length")
        #    )
        # )
    except urllib.error.HTTPError:
        sys.stdout.write("HTTP-Error: maybe the station id is incorrect\n")
        sys.exit(-1)
    except urllib.error.URLError:
        sys.stdout.write(
            "URL-Error: maybe internet connection is not yet established\n"
        )
        raise

    return response.read()


def hour_to_file(time_string, station):

    with open(TIME_FILE_PATH + f"_{station}", "w") as f:
        f.write(time_string)


def get_hour_from_file(station):

    p = Path(TIME_FILE_PATH + f"_{station}")

    if not p.exists():
        hour_to_file("-1", station)

    with open(TIME_FILE_PATH + f"_{station}") as reader:
        hour = reader.read()

    return hour


def get_actual_measurement_time(actual_time):
    """This function calculates the actual time the measurement was taken by the dwd.

    For example the measurement was taken 11:00 (UTC) and today is the 2020-11-27.
    The measurment was fetched from server 11:40, writing 11:40 to the database
    is wrong.
    """
    act_hour = actual_time[0]
    act_min = actual_time[1]

    # get current time in utc
    unow = datetime.utcnow()
    # build timestring
    timestring = "{}-{}-{} {}:{}".format(
        unow.year, unow.month, unow.day, act_hour, act_min
    )
    # get epoch time from timestring
    actual_epoch = calendar.timegm(time.strptime(timestring, "%Y-%m-%d %H:%M"))

    return actual_epoch


def get_values(wff, station):

    time_dwd_measurement = wff[3][1].split(":")[0]  # hour in UTC
    time_last_measurement = get_hour_from_file(station)

    if time_dwd_measurement == time_last_measurement:
        sys.exit(2)  # no new measurement available on the dwd open data platform

    if wff[3][9].startswith("--"):
        sys.stderr.write("ERROR: no values are available for this hour\n")
        sys.exit(1)
    else:
        interesting_values = {
            "temperature": wff[3][9],
            "cloudcover": wff[3][2],
            "visibility": wff[3][14],
            "maxwind": wff[3][18],
            "winddirection": wff[3][22],
            "precipitation": wff[3][33],
            "pressure": wff[3][36],
            "humidity": wff[3][37],
        }
        hour_to_file(time_dwd_measurement, station)

    actual_measurement_time = get_actual_measurement_time(wff[3][1].split(":"))

    return (interesting_values, actual_measurement_time)


def write_values_to_db(vals, station):

    url = "http://127.0.0.1:8086/write?db=dwd&precision=s"

    values = vals[0]
    actual_time = vals[1]

    payload = ""
    for key in values:
        payload += "{},stationid={} value={} {}\n".format(
            key, station, values[key].replace(",", "."), actual_time
        )

    headers = {"content-encoding": "application/x-www-form-urlencoded"}
    r = requests.post(url, data=payload, headers=headers)

    if r.status_code != 204:
        sys.stderr.write("ERROR: unable to send values to InfluxDB\n")
        print(r.status_code, r.reason)
        print(r.text + "\n")


def get_station_file():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-s",
        "--station",
        required=True,
        help="id of the weather station (mandatory)",
    )

    args = parser.parse_args()

    if args.station is None:
        sys.stderr.write("ERROR: station id needed\n")
        sys.exit(1)
    else:
        station = args.station
        if len(station) < 5:
            station += "_"

    return station + "-BEOB.csv", station


def read_weather(station_conf):

    is_ok = True

    try:
        wf = get_file_from_url(DWD_POI_URL + station_conf[0])
        # wf = get_test_file()
    except urllib.error.URLError:
        wf = None
        is_ok = False

    if wf is not None:
        wff = wf.decode("utf_8").splitlines()
        csv_reader = csv.reader(wff, delimiter=";")
        wff = [row for row in csv_reader]
        write_values_to_db(get_values(wff, station_conf[1]), station_conf[1])
    else:
        sys.stderr.write("ERROR: unable to fetch weather file from dwd\n")

    return is_ok


def main():

    station_conf = get_station_file()
    read_weather(station_conf)


if __name__ == "__main__":
    main()
