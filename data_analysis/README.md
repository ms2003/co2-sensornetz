# Datenanalyse

## dwd-fetcher

**dwd-fetcher** ist ein Python-Skript was Wetterdaten einer beliebigen DWD-Wetterstation
von openData-portal des Deutschen Wetterdienstes herunterlädt und eine Auswahl an
Wetterdaten in die InfluxDB des Projektes schreibt.

```
./dwd-fetcher.py -s 10170
```

10170 ist der Stations-Code des DWD für die Wetterstation in Warnemünde.


## sensor-validation

Die Diagramme, welche zur Validierung der Messergebnisse herangezogen wurden können
mit dem tool `plot_scatter.py` erzeugt werden. Der Aufruf

```
python plot_scatter.py samples/2020-12-23
```

erzeugt die datei `validation.png`.


