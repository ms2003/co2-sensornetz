using thc.Entities;
using Microsoft.EntityFrameworkCore;

namespace thc.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<AppUser> Users { get; set; }
    }
}