using thc.Entities;

namespace thc.Interfaces
{
    public interface ITokenService
    {
        string CreateToken(AppUser user);
    }
}