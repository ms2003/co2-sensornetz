using Microsoft.AspNetCore.Mvc;

namespace thc.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseApiController : ControllerBase
    {
    }
}