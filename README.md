# CO2-Sensornetz Projekt

<p align="center">
<a href="https://git.informatik.uni-rostock.de/ms2003/co2-sensornetz/-/blob/master/LICENSE" title="License">
<img src="https://img.shields.io/badge/License-Apache%202.0-green.svg?style=flat"></a>
</p>

Das CO2-Sensornetz Projekt hat zum Ziel eine offene Infrastruktur zuschaffen, um eine
große Anzahl an günstigen C02-Sensoren zu verwalten. Das Projekt besteht aus mehreren
Soft- und Hardwarelösungen.


## THC - Der zentrale Server

**T**he **H**uge **C**ollector soll die Messpunkte aller Sensoren entgegennehmen, in
einer *time series database* (TSDB) abspeichern und eine sichere Schnittstelle bieten,
um externen Applikationen, wie der CO2WarnApp, Zugriff auf bestimmte Messpunkte zu
bieten.

[Dokumentation zum THC-Server](thc/README.md)

## Sensoren

Die Sensoren verwenden einen ESP32-Microcontrollerm an dem ein eCO2-Sensor und ein
Umweltsensor angeschlossen ist. Messpunkte werden per HTTPS an den zentralen Server (THC)
übermittelt. Des weiteren sendet jeder Sensor einen zufälligen Raum-Identifikator
über BLE aus.

[Dokumentation zum Sensor](sensor/README.md)

## CO2WarnApp

Die für Anroid-Smartphones entwickelte Applikation CO2WarnApp soll in der Lage sein,
die von den Sensoren ausgesendeten BLE-Signale zu empfangen und die Messwerte der
Sensoren bem THC zu erfragen um dann gegebenenfalls eine Benachrichtigung auszulösen,
falls die Messwerte einen gewissen Schwellenwert überschreiten.

[Dokumentation zur CO2WarnApp](CO2WarnApp/README.md)


## Datenanalyse

Die Daten sollen natürlich einer Analyse unterzogen werden, hoffentlich kann man
etwas daraus lernen. Um Korrelationen mit Wetter außerhalb eines Gebäude zu ermöglichen
werden seit dem 27.11.2020 einige Wetterdaten der warnemünder Wetterstation des
DWD erhoben und in die TSDB geschrieben.

[Dokumentation der Werkezuge zu Datenanalyse ](data_analysis/README.md)
