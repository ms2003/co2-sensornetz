# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
This project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Example  [0.1.0-alpha] - 2020-11-01

### Added

- Test for benchmarking the application

### Fixed

- replaced a database trigger with a daily executed thread for cleaning up the database

### Changed

- Featre xy wurde entfernt
