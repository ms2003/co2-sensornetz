# CO2WarnApp

Dokumentation für die CO2WarnApp aus dem CO2-Sensornetz-Projekt.

**DEMO:**

![CO2WarnApp Demo](CO2WarnApp.mp4)


## TODO

1. [x] THC-Login
2. [x] Manuelle RoomID-Eingabe
3. [x] RoomID-Eingabe mittels QR-Code
4. [x] RoomID-Eingabe mittels BLE-Beacons
5. [x] RoomID-Eingabe mittels "Teilen" aus externen Apps
6. [x] THC-Datenabfrage
7. [x] Notifications
