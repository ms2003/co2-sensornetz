package de.nachtsieb.co2warnapp.ble;

public class BLEIdentity {

    private String roomID;
    private String mac;
    private int rssi;
    private boolean selected;

    public BLEIdentity(String roomID, String mac, int rssi) {
        this.setRoomID(roomID);
        this.setMac(mac);
        this.setRssi(rssi);
        this.selected = false;
    }

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public void select() {
        this.selected = true;
    }

    public void unselect() {
        this.selected = false;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected (boolean b) {
        this.selected = b;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public boolean hasSameMac(BLEIdentity id) {
        return this.getMac().equals(id.getMac());
    }

    public String toString() {
        String s = String.format("%s\n%s (%d dBM)\n",
                this.getRoomID(), this.getMac(), this.getRssi());
        return s;
    }


}
