package de.nachtsieb.co2warnapp.exceptions;

public class CO2UnauthorizedException extends Exception {

    public CO2UnauthorizedException(String message) {
        super(message);
    }

    public CO2UnauthorizedException() {

    }
}
