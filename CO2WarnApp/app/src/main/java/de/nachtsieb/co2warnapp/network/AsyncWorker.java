package de.nachtsieb.co2warnapp.network;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import de.nachtsieb.co2warnapp.MainActivity;
import de.nachtsieb.co2warnapp.entities.DataPoint;
import de.nachtsieb.co2warnapp.entities.THCLoginResponse;

/**
 * AsyncWorker using the RestManager to perform a login if necessary and retrieve latest
 * measurements from THC.*
 */
public class AsyncWorker extends AsyncTask<String, Integer, DataPoint> {

    private static final String TAG = "AsyncWorker";

    private static final long   TOKEN_VALID_FOR_MILLIS = 60* 60 * 1000; // one hour
    private static final int    MAX_LOGIN_ATTEMPTS = 5;
    private static final int    TIME_BETWEEN_RETRIES = 5000; // 5 seconds

    private MainActivity mainActivity;
    private String roomID;

    @Override
    protected DataPoint doInBackground(String... strings) {

        THCLoginResponse login;
        login = mainActivity.getLoginResponse();

        int retryAttempt = 0;
        long currrTime = System.currentTimeMillis();

        DataPoint dataPoint = null;

        RestManager rest = new RestManager();

        // do login if necessary
        if (login == null || login.getLoginTime() + TOKEN_VALID_FOR_MILLIS < currrTime) {
            login = rest.login();

            if (login == null) {
                Log.w(TAG, "unable to login");
                return null;
            } else {
                login.setLoginTime(currrTime);
                mainActivity.setLoginResponse(login);
                Log.i(TAG, "Login successful");
            }
        }

        // request data from server
        dataPoint = rest.getLastMeasurement(roomID, login);


        // retry for MAX_LOGIN_ATTEMPTS
        try {

            while (retryAttempt < MAX_LOGIN_ATTEMPTS && dataPoint == null) {

                retryAttempt++;

                Log.i(TAG, "Retry for the " + retryAttempt + " time");
                Thread.sleep(TIME_BETWEEN_RETRIES);

                if (login != null) {

                    login = rest.login();
                    if (login == null) {
                        Log.d(TAG, "Login not successful.");
                        continue;
                    }
                }

                Log.i(TAG, "Login succeeded, try to fetch data.");
                login.setLoginTime(currrTime);
                mainActivity.setLoginResponse(login);
                dataPoint = rest.getLastMeasurement(roomID, login);
            }

        } catch (InterruptedException e) {
            Log.e(TAG,"Interrupted while sleeping");
            return null;
        }

        return dataPoint;
    }

    @Override
    protected void onPostExecute(DataPoint dataPoint) {
        if (dataPoint == null)
            Toast.makeText(mainActivity, "No data could be fetched", Toast.LENGTH_LONG).show();
        else
            mainActivity.setData(dataPoint);
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }
}
