package de.nachtsieb.co2warnapp.network;

import android.os.AsyncTask;

import android.util.Log;

import de.nachtsieb.co2warnapp.entities.DataPoint;
import de.nachtsieb.co2warnapp.entities.THCLoginResponse;
/**
 * A dirty copy of the AsyncWorker for fetching the DataPoints from the AlarmReceiver
 *
 * -> currently every transmission performs a login.
 *
 */

public class AsyncFetcher extends AsyncTask<String, Integer, DataPoint> {

    private static final String TAG = "AsyncFetcher";

    private static final long   TOKEN_VALID_FOR_MILLIS = 60* 60 * 1000; // one hour
    private static final int    MAX_RETRY_ATTEMPTS = 5;
    private static final int    TIME_BETWEEN_RETRIES = 500; // 0.5 seconds

    private String roomID;

    @Override
    protected DataPoint doInBackground(String... strings) {

        THCLoginResponse login = null;

        int retryAttempt = 0;
        long currrTime = System.currentTimeMillis();

        DataPoint dataPoint = null;

        RestManager rest = new RestManager();

        login = rest.login();

        if (login == null) {
            Log.w(TAG, "unable to login");
            return null;
        } else {
            login.setLoginTime(currrTime);
            Log.i(TAG, "Login successful");
        }

        // request data from server
        dataPoint = rest.getLastMeasurement(roomID, login);


        // retry for MAX_LOGIN_ATTEMPTS
        try {

            while (retryAttempt < MAX_RETRY_ATTEMPTS && dataPoint == null) {

                retryAttempt++;

                Log.i(TAG, "Retry for the " + retryAttempt + " time");
                Thread.sleep(TIME_BETWEEN_RETRIES);

                if (login != null) {

                    login = rest.login();
                    if (login == null) {
                        Log.d(TAG, "Login not successful.");
                        continue;
                    }
                }

                Log.i(TAG, "Login succeeded, try to fetch data.");
                login.setLoginTime(currrTime);
                dataPoint = rest.getLastMeasurement(roomID, login);
            }

        } catch (InterruptedException e) {
            Log.e(TAG,"Interrupted while sleeping");
            return null;
        }

        return dataPoint;
    }

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }
}
