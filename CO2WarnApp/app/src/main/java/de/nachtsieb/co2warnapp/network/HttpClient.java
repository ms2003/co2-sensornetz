package de.nachtsieb.co2warnapp.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import de.nachtsieb.co2warnapp.exceptions.CO2UnauthorizedException;

class HttpClient {

    private static final String USER_AGENT = "co2warnapp";

    private URL url;

    HttpClient() {};

    HttpClient(String url) {
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public String get(String requestURL) throws CO2UnauthorizedException {

        String response = null;

        try {

            URL url = new URL(requestURL);

            HttpURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json; charset=UTF-8");
            connection.setRequestProperty("User-Agent",USER_AGENT);

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));

                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
				    sb.append(inputLine);
			    }

			    in.close();
                response = sb.toString();

            } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                throw new CO2UnauthorizedException();
            }

        }catch (IOException e){
            e.printStackTrace();
            //TODO
        }

        return response;
    }

    public String sendPost(String payload){

        String responsePayload = "";
        try {
            HttpURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("User-Agent",USER_AGENT);

            byte[] bytes = payload.getBytes("UTF-8");
            connection.setFixedLengthStreamingMode(bytes.length);

            OutputStream out = connection.getOutputStream();

            out.write(bytes);
            out.flush();
            out.close();

            //DEBUG
            int responseCode = connection.getResponseCode();
            System.out.println("POST Response Code : " + responseCode);
            System.out.println("POST Response Message : " + connection.getResponseMessage());

            //getting response
            String responseLine;
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            while ((responseLine = reader.readLine()) != null) {
                responsePayload += responseLine.toString();
            }

            System.out.println(responsePayload);

        }catch (IOException e){
            e.printStackTrace();
            //TODO
        }

        return (responsePayload.length() <= 1) ? null : responsePayload;
    }
}
