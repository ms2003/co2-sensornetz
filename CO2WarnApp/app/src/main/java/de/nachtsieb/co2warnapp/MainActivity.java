package de.nachtsieb.co2warnapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.ShareCompat;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;

import de.nachtsieb.co2warnapp.entities.DataPoint;
import de.nachtsieb.co2warnapp.entities.THCLoginResponse;
import de.nachtsieb.co2warnapp.network.AsyncWorker;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static final int NOTIFICATION_WARN_ID = 200;
    public static final int NOTIFICATION_OK_ID = 400;
    public static final int NOTIFICATION_DURATION = 60 * 60 * 1000; // 60 minutes in millis

    private MainActivity outerRef;
    private THCLoginResponse loginResponse;
    private TextView txtData;

    private AlarmManager alarmManager;
    private PendingIntent alarmIntent;
    private Intent intentInsideAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set up notification channels
        raiseChannels();

        /* ROOMID */
        TextView txtRoomID = findViewById(R.id.txtRoomID);
        String roomID = "eee10b1c-4076-11eb-9446-c02caffebabe";
        txtRoomID.setText(roomID);



        Button buttonManual = findViewById(R.id.buttonManual);
        Button buttonQR = findViewById(R.id.buttonQRCode);
        Button buttonBLE = findViewById(R.id.buttonBLE);

        Button buttonLogin = findViewById(R.id.buttonLogin);

        outerRef = this;
        buttonManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentInput = new Intent(outerRef, ManualActivity.class);
                startActivity(intentInput);
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scheduleAlarm();
                String roomID = txtRoomID.getText().toString();
                if (roomID.isEmpty()) {
                    Toast.makeText(outerRef, "No roomID entered.", Toast.LENGTH_LONG).show();
                    return;
                }
                Toast.makeText(outerRef, "Monitoring started for 60 minutes.", Toast.LENGTH_LONG).show();

                // TODO fetch data only for debug reasons
                AsyncWorker worker = new AsyncWorker();
                worker.setMainActivity(outerRef);
                worker.setRoomID(roomID);
                worker.execute();
                return;
            }
        });

        txtRoomID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtRoomID.setText("No RoomID");
            }
        });

        txtData = findViewById(R.id.textData);
        txtData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String roomID = txtRoomID.getText().toString();

                if (roomID.isEmpty()) {
                    Toast.makeText(outerRef, "No roomID entered.", Toast.LENGTH_LONG).show();
                    return;
                }

                AsyncWorker worker = new AsyncWorker();
                worker.setMainActivity(outerRef);
                worker.setRoomID(roomID);
                worker.execute();
                return;
            }
        });


        // display the received roomID from ManualActivity or BLEScanActivity
        Intent inputIntent = getIntent();
        if (inputIntent != null)
            roomID = inputIntent.getStringExtra(ManualActivity.ROOM_ID);

        final ShareCompat.IntentReader reader = ShareCompat.IntentReader.from(this);
        if (reader.isShareIntent())
            roomID = reader.getText().toString();

        txtRoomID.setText(roomID);

        /* QR-CODE */
        buttonQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 0);
                } catch (Exception e) {
                    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }
            }
        });


        /* BLE */

        buttonBLE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bleIntent = new Intent(outerRef, BLEScanActivity.class);
                startActivity(bleIntent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        TextView txtRoomID = findViewById(R.id.txtRoomID);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {

            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                txtRoomID.setText(contents);
            }
            if (resultCode == RESULT_CANCELED) {
                //TODO
            }
        }
    }

    /* set up notification channels */
    private void raiseChannels() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            CharSequence name = "CO2-Warnung";
            String descr = "Warnung bei Überschreitung von 1450 ppm C02 Konzentration";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel chan = new NotificationChannel("CO2-WARN", name, importance);
            chan.setDescription(descr);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(chan);

            name = "CO2-Entwarnung";
            descr = "Warnung bei CO2 Konzentration unter 600 ppm";
            importance = NotificationManager.IMPORTANCE_DEFAULT;
            chan = new NotificationChannel("CO2-OK", name, importance);
            chan.setDescription(descr);
            notificationManager.createNotificationChannel(chan);

        }
    }
    void scheduleAlarm() {

        // kill the old alarm first
        if (this.alarmIntent != null && intentInsideAlarm != null) {
            Log.v(TAG, "CANCEL OLD ALARM");
            cancelAlarm(alarmIntent, intentInsideAlarm); //TODO: not working
        }

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);

        intent.putExtra(AlarmReceiver.KEY_ROOM_ID, getRoomID());
        intent.putExtra(
                AlarmReceiver.KEY_NOTIFICATION_DURATION,
                System.currentTimeMillis() +  NOTIFICATION_DURATION);

        alarmIntent = PendingIntent.getBroadcast(
                getApplicationContext(),
                0,
                intent,
                0);


        alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 10 * 1000,
                alarmIntent);

        intentInsideAlarm = intent;
    }

    void cancelAlarm(PendingIntent pi, Intent i) {

        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getApplicationContext(),
                1253,
                i,
                PendingIntent.FLAG_UPDATE_CURRENT|  Intent.FILL_IN_DATA);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarmManager.cancel(pendingIntent);

    }


    public synchronized THCLoginResponse getLoginResponse() {
        return loginResponse;
    }

    public synchronized void setLoginResponse(THCLoginResponse loginResponse) {
        this.loginResponse = loginResponse;
    }

    public synchronized void setData(DataPoint dp) {
        txtData = findViewById(R.id.textData);
        txtData.setText(dp.toString());
    }

    public String getRoomID() {
        TextView txtRoomID = findViewById(R.id.txtRoomID);
        return txtRoomID.getText().toString();
    }
}