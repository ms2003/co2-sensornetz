package de.nachtsieb.co2warnapp.entities;

public class THCLogin {

    public THCLogin() {};

    public THCLogin(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    };

    private String password;
    private String username;
    //private String organization;
    //private String institute;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
