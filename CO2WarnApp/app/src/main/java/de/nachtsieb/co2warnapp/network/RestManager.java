package de.nachtsieb.co2warnapp.network;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.nachtsieb.co2warnapp.entities.DataPoint;
import de.nachtsieb.co2warnapp.entities.THCLogin;
import de.nachtsieb.co2warnapp.entities.THCLoginResponse;
import de.nachtsieb.co2warnapp.exceptions.CO2UnauthorizedException;

public class RestManager {

    private static final String TAG = "RestManager";


    public final String         BASE_URL = "https://thc.nachtsieb.de/api/";
    public final String         BASE_LOGIN_URL = BASE_URL + "client/login";
    public final String         BASE_MEASUREMENT_URL = BASE_URL + "client/measurement/";

    private ObjectMapper objMapper;

    public RestManager() {
        this.objMapper = new ObjectMapper();
    };

    public THCLoginResponse login() {
        return this.login("thc", "admin");
    }

    public THCLoginResponse login(String username, String password) {

        THCLogin thcLogin = new THCLogin(username, password);
        HttpClient client = new HttpClient(BASE_LOGIN_URL);
        THCLoginResponse thcLoginResponse = null;

        try {

            String jsonPayload = objMapper.writeValueAsString(thcLogin);
            String response = client.sendPost(jsonPayload);
            thcLoginResponse = objMapper.readValue(response, THCLoginResponse.class);

        } catch (JsonProcessingException e) {
            Log.e(TAG, "Unable to process login response");
        }

        return thcLoginResponse;
    }

    public DataPoint getLastMeasurement(String roomID, THCLoginResponse loginResponse) {


        String response = null;
        DataPoint dp = null;
        String requestURL = BASE_MEASUREMENT_URL
                + roomID
                + "?access_token=" + loginResponse.getToken();

        HttpClient client = new HttpClient();

        try {

            response = client.get(requestURL);
            Log.v(TAG, response);
            dp = objMapper.readValue(response, DataPoint.class);

        } catch (JsonProcessingException e) {
            Log.e(TAG, "Unable to process measurement response");
            return null;
        } catch (CO2UnauthorizedException e) {
            Log.e(TAG, "Unable to fetch measurement: unauthorized");
            return null;
        }
        return dp;
    }
}
