package de.nachtsieb.co2warnapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ManualActivity extends AppCompatActivity {

    public final static String ROOM_ID = "de.nachtsieb.co2warnapp.ROOM_ID";
    private String roomID = "eee10b1c-4076-11eb-9446-c02caffebabe";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roomid_input);
        EditText editInput =  findViewById(R.id.txtInputRoomID);
        editInput.setText(roomID);
    }

    // onClick handler added in layout xml
    public void SubmitRoomID(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        String msgRoomID = ((EditText) findViewById(R.id.txtInputRoomID)).getText().toString();
        intent.putExtra(ROOM_ID, msgRoomID);
        startActivity(intent);
    }
}