package de.nachtsieb.co2warnapp;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.nachtsieb.co2warnapp.ble.BLEIdentity;
import de.nachtsieb.co2warnapp.ble.ScanCallback;

public class BLEScanActivity extends AppCompatActivity {

    BLEScanActivity outerRef = this;
    ListView listViewBLE;
    public ArrayAdapter<BLEIdentity> adapter;

    List<BLEIdentity> identities = new ArrayList<>();
    List<BLEIdentity> choosedIdentities = new ArrayList<>();
    List<CheckBox> checkBoxes = new ArrayList<>();

    BluetoothAdapter bleAdapter;
    ScanCallback bleScanCallback;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ble_scan);

        checkBoxes.clear();

        Button buttonSubmit = findViewById(R.id.buttonBLESubmit);
        Button buttonScan = findViewById(R.id.buttonBLEScan);



        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // check if BLE exists
                if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
                    Toast.makeText(outerRef, "This phone does not support BLE!", Toast.LENGTH_SHORT).show();
                    finish();
                }

                BluetoothManager bleManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                bleAdapter = bleManager.getAdapter();
                if (bleAdapter == null) {
                    Toast.makeText(outerRef, "Creation of BLE adapter not possible.", Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }

                if (bleAdapter.isEnabled()){
                    bleScanCallback = new ScanCallback(outerRef);
                    identities.clear();
                    scanDevices();
                }

            }
        });

        // test values
        identities.add(new BLEIdentity("39b0af91-cc7d-4d2c-9e28-co2caffebabe", "3b:4f:31:b4:c8:26", -68));
        identities.add(new BLEIdentity("eb91c458-ea9d-4472-a329-co2caffebabe", "77:43:83:c3:e1:2f", -78));
        identities.add(new BLEIdentity("e9a63ed2-04c1-4a88-98a8-co2caffebabe", "63:99:5b:47:5d:58", -91));

        adapter = new ArrayAdapter<BLEIdentity>(
                this,
                R.layout.list_widget,
                R.id.textView1,
                identities) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView text = view.findViewById(R.id.textView1);
                CheckBox box = view.findViewById(R.id.checkbox);
                checkBoxes.add(box);

                BLEIdentity currentIdentity = identities.get(position);

                text.setText(currentIdentity.toString());

                // change color depending on approx distance
                if (currentIdentity.getRssi() >= -70)
                    text.setTextColor(Color.parseColor("#00FF00"));
                else if (currentIdentity.getRssi() < -70 && currentIdentity.getRssi() > -90)
                    text.setTextColor(Color.parseColor("#FFA31A"));
                else
                    text.setTextColor(Color.parseColor("#FF0000"));

                box.setChecked(currentIdentity.isSelected());
                box.setTag(R.id.keyStore, identities);
                box.setTag(R.id.keyPosition, position);

                box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        List<BLEIdentity> identities = (List<BLEIdentity>) buttonView.getTag(R.id.keyStore);
                        int pos = (int) buttonView.getTag(R.id.keyPosition);
                        identities.get(pos).setSelected(isChecked);
                    }
                });

                return view;
            }
        };
        listViewBLE = (ListView) findViewById(R.id.listViewBLE);
        listViewBLE.setAdapter(adapter);

        /* SUBMIT RoomID */
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(BLEIdentity identity : identities) {
                    if (identity.isSelected()) {
                        choosedIdentities.add(identity);
                    }
                }

                if (choosedIdentities.size() == 0) {
                    Toast.makeText(outerRef, "No RoomID selected", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (choosedIdentities.size() > 1) {
                    Toast.makeText(
                            outerRef,
                            "Currently only one RoomID is supported",
                            Toast.LENGTH_SHORT).show();

                    // unselect all selected identities
                    for (BLEIdentity identity : choosedIdentities) {
                        identity.unselect();
                    }
                    choosedIdentities.clear();
                    for (CheckBox box : checkBoxes) {
                        box.setChecked(false);
                    }
                    return;
                }

                // send room id to MainActivity
                Intent intent = new Intent(outerRef, MainActivity.class);
                intent.putExtra(ManualActivity.ROOM_ID, choosedIdentities.get(0).getRoomID());
                startActivity(intent);
            }
        });
    }


    private void scanDevices() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                bleAdapter.stopLeScan(bleScanCallback);
            }
        }, 5000);
        bleAdapter.startLeScan(bleScanCallback);
    }
}