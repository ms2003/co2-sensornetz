package de.nachtsieb.co2warnapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.SystemClock;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import de.nachtsieb.co2warnapp.entities.DataPoint;
import de.nachtsieb.co2warnapp.network.AsyncFetcher;

public class AlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "ALARM";

    public final static String KEY_ROOM_ID = "de.nachtsieb.co2warnapp.ROOMID";
    public final static String KEY_NOTIFICATION_DURATION = "de.nachtsieb.co2warnapp.NOT_DURA";
    public final static String KEY_NOTIFICATION_STATE = "de.nachtsieb.co2warnapp.NOT_STATE";

    private final int STATE_OK = 1;
    private final int STATE_WARN = 2;

    private long notificationDuration;
    private String roomID;
    private int thisState;

    @Override
    public void onReceive(Context context, Intent intent) {

        roomID = intent.getStringExtra(KEY_ROOM_ID);
        notificationDuration = intent.getLongExtra(KEY_NOTIFICATION_DURATION, 0);

        int lastState = intent.getIntExtra(KEY_NOTIFICATION_STATE, STATE_OK);


        Log.v(TAG,"ALARM ALARM ALARM for ROOM: " + roomID);

        AsyncFetcher worker = new AsyncFetcher();
        worker.setRoomID(roomID);
        worker.execute();

        DataPoint dp = null;

        try {

            dp = worker.get(5, TimeUnit.SECONDS);

        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG, "Unable to fetch data point");
        } catch (TimeoutException e) {
            Log.e(TAG, "fetching data point timed out");
        }

        // throw correct notification
        if (dp != null && dp.co2BelowLimit()) {
            thisState = STATE_OK;
            if (lastState == STATE_WARN) {showCO2OkNotification(context, dp);}
        } else {
            thisState = STATE_WARN;
            showCO2WarningNotification(context, dp);
        }

        if (System.currentTimeMillis() < notificationDuration) {
            long secondsToGo = (notificationDuration - System.currentTimeMillis()) / 1000;
            Log.v(TAG,"Rescheduled alarm; Seconds until end: " + secondsToGo);
            reschedule(context, intent);
        }
        else {
            Log.v(TAG, "Notification duration ended, no further notification for: " + roomID);
        }
    }

    void reschedule(Context context, Intent intent) {
        AlarmManager alarmManager;
        PendingIntent alarmIntent;
        alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

        intent.putExtra(AlarmReceiver.KEY_ROOM_ID, roomID);
        intent.putExtra(
                AlarmReceiver.KEY_NOTIFICATION_DURATION,
                notificationDuration);
        intent.putExtra(KEY_NOTIFICATION_STATE, thisState);


        alarmIntent = PendingIntent.getBroadcast(
                context,
                0,
                intent,
                0);

        alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 60 * 1000,
                alarmIntent);

    }

    /* notification co2-ok mit id 400 */
    private void showCO2OkNotification(Context context, DataPoint dp) {
        Resources res = context.getResources();
        int color = res.getColor(R.color.teal_200);

        final String notificationText = new String(
                String.format(
                        "Raum: %s\nCO2-Konzentration in Ordnung (%s ppm).",
                        dp.getRoomId(),
                        dp.getCo2Concentration()));

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, "CO2-OK")
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle("CO2-OK")
                        .setContentText(notificationText)
                        .setColor(color)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(notificationText))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManagerCompat notManager = NotificationManagerCompat.from(context);
        notManager.notify(MainActivity.NOTIFICATION_OK_ID, mBuilder.build());
    }

    private void showCO2WarningNotification(Context context, DataPoint dp) {

        Resources res = context.getResources();
        int color = res.getColor(R.color.purple_500);

        final String notificationText = new String(
                String.format(
                        "Raum: %s\nCO2-Konzentration überschritten (%s ppm).",
                        dp.getRoomId(),
                        dp.getCo2Concentration()));

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, "CO2-WARN")
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle("CO2-WARNUNG!")
                        .setContentText(notificationText)
                        .setColor(color)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(notificationText))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManagerCompat notManager = NotificationManagerCompat.from(context);
        notManager.notify(MainActivity.NOTIFICATION_WARN_ID, mBuilder.build());
    }

}
