package de.nachtsieb.co2warnapp.entities;

import androidx.annotation.NonNull;

import java.util.UUID;

public class DataPoint {

    public static final int CO2_LIMIT = 1450;

    public DataPoint() {};

    public DataPoint(
            UUID roomID,
            long timestamp,
            float temperature,
            float humidity,
            int co2Concentration,
            int tvoc)
    {
        this.setRoomId(roomID);
        this.setTemperature(temperature);
        this.setHumidity(humidity);
        this.setCo2Concentration(co2Concentration);
        this.setTvoc(tvoc);
        this.setTimestamp(timestamp);
    }


    private UUID roomId;
    private float temperature;
    private float humidity;
    private float pressure;
    private int co2Concentration;
    private int tvoc;
    private long timestamp;


    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public int getTvoc() {
        return tvoc;
    }

    public void setTvoc(int tvoc) {
        this.tvoc = tvoc;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public int getCo2Concentration() {
        return co2Concentration;
    }

    public void setCo2Concentration(int co2Concentration) {
        this.co2Concentration = co2Concentration;
    }

    public boolean co2BelowLimit() {
        return co2Concentration < CO2_LIMIT;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

		sb.append(String.format("%-10s %10.2f %s\n","Temp:", temperature, "°C"));
		sb.append(String.format("%-10s %10.2f %s\n","Humid:", humidity, "%"));
		sb.append(String.format("%-10s % 10d %s\n","CO2:", co2Concentration, "ppm"));
		sb.append(String.format("%-10s % 10d %s\n","tvoc:", tvoc, "ppb"));

		return sb.toString();
    }
}
