package de.nachtsieb.co2warnapp.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.nachtsieb.co2warnapp.BLEScanActivity;

public class ScanCallback implements BluetoothAdapter.LeScanCallback {

    List<BLEIdentity> alreadyKnownIdentities = new ArrayList<>();

    BLEScanActivity bleScanActivity;
    public ScanCallback(BLEScanActivity activity) {
        super();
        this.bleScanActivity = activity;
    }

    public String getUUIDFromBytes(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        UUID uuid = new UUID(bb.getLong(), bb.getLong());
        return uuid.toString();
    }

    public boolean notAlreadyKnown(BLEIdentity id) {

        for (BLEIdentity knownIdentity : alreadyKnownIdentities) {
            if (knownIdentity.hasSameMac(id)) {return false;};
        }
        return true;
    }

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {

        final BluetoothDevice dev = device;
        bleScanActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                byte[] uuidBytes = new byte[16];
                String s = "longer than 0";
                int length = scanRecord.length;

                // extract UUID from payload
                int offset = 9; // uuid starts after 8 bytes in the iBeacon protocol
                for (int i = 0 ; i < 16 ; i++) {
                    uuidBytes[i] = scanRecord[i + offset];
                }

                // change endianness of the UUID
                byte[] uuidBigEndian = new byte[16];
                int k = 0;
                for (int i = 15 ; i >= 0 ; i--) {
                    uuidBigEndian[i] = uuidBytes[k];
                    k++;
                }

                String uuidString = getUUIDFromBytes(uuidBigEndian);

                BLEIdentity identity = new BLEIdentity(uuidString, dev.getAddress(), rssi);

                if ( alreadyKnownIdentities.isEmpty() || notAlreadyKnown(identity)) {
                    if (uuidString.toLowerCase().endsWith("c02caffebabe")) {
                        alreadyKnownIdentities.add(identity);
                        bleScanActivity.adapter.add(identity);
                        bleScanActivity.adapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }
}
