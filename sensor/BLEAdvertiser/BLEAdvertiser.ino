#include<SensorSetup.h>
#include<SensorBLE.h>

/**** local configuration ****/

// wifi credentials for setup phase
const char AP_SSID[] = "sensor-wifi";
const char AP_PASS[] = "password";
#define SETUP_PHASE_SECS    240

/*****************************/

/******** globals ************/
String roomID;
/*****************************/

/******** SETUP ************/

void setup() {

    Serial.begin(115200);
    delay(4000);
    NVS.begin(); // Non-Volatile-Storage manager
    SensorSetup sensor;

    if(sensor.isFirstStart()) {

        sensor.setAP(AP_SSID, AP_PASS);
        sensor.start(SETUP_PHASE_SECS, true); // start setup phase for SETUP_PHASE_SECS seconds
        //printConfigParameter();
        ESP.restart();

    } else {

        // if you want to reconfigure run 'python esptool.py erase_flash'
        Serial.println("not the first start .. continuing ...");

        roomID = NVS.getString(K_ROOM_ID);
        SensorBLE::advertise(roomID, "co2", 0);
    }
}

void loop() {}
