#define SCD30  false

#include<SensorSetup.h>
#include<SensorData.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>

/**** local configuration ****/
// wifi credentials for setup phase
const char AP_SSID[] = "sensor-wifi";
const char AP_PASS[] = "password";
#define SETUP_PHASE_SECS    240
#define WARMUP_TIME         15  // minutes
const bool isBME680 = true;     // false -> CCS811 is used

/*****************************/

/******** globals ************/

// parallel processing of lora and sensor
TaskHandle_t sensorTask;
SemaphoreHandle_t semaphore;

SensorData sd;
String roomID;
String dataString;

// LoRa specific
String loraAppKey;
String loraDevEUI;

// the CO2-TTN-APP identifier
static const u1_t PROGMEM APPEUI[8]= { 0x45, 0xAF, 0x03, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };
static u1_t PROGMEM DEVEUI[8];
static u1_t PROGMEM APPKEY[16];

static osjob_t sendjob;

const unsigned TX_INTERVAL = 60; // send data every 60 seconds

uint8_t mydata[23]; // send 23 byte to the things network

const lmic_pinmap lmic_pins = {
    .nss = 18,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,
    .dio = {26, 33, 32},
};

/*****************************/

// copy keys and euis to flash
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
void os_getDevKey (u1_t* buf) { memcpy_P(buf, APPKEY, 16);}

void printHex2(unsigned v) {
    v &= 0xff;
    if (v < 16)
        Serial.print('0');
    Serial.print(v, HEX);
}

/****************  pseudo-hex conversion ****************/
void unHex(const char* inP, byte* outP, size_t len) {
  for (; len > 1; len -= 2) {
    byte val = asc2byte(*inP++) << 4;
    *outP++ = val | asc2byte(*inP++);
  }
}

byte asc2byte(char chr) {
  byte rVal = 0;
  if (isdigit(chr)) {
    rVal = chr - '0';
  } else if (chr >= 'A' && chr <= 'F') {
    rVal = chr + 10 - 'A';
  }
  return rVal;
}
/******************************************************/

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            {
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              /*
              Serial.print("netid: ");
              Serial.println(netid, DEC);
              Serial.print("devaddr: ");
              Serial.println(devaddr, HEX);
              Serial.print("AppSKey: ");

              for (size_t i=0; i<sizeof(artKey); ++i) {
                if (i != 0)
                  Serial.print("-");
                printHex2(artKey[i]);
              }
              Serial.println("");
              Serial.print("NwkSKey: ");
              for (size_t i=0; i<sizeof(nwkKey); ++i) {
                      if (i != 0)
                              Serial.print("-");
                      printHex2(nwkKey[i]);
              }
              Serial.println();
              */
            }
            // Disable link check validation (automatically enabled
            // during join, but because slow data rates change max TX
            // size.
            LMIC_setLinkCheckMode(0);
            break;

        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.print(F("Received "));
              Serial.print(LMIC.dataLen);
              Serial.println(F(" bytes of payload"));
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
        case EV_TXSTART:
            Serial.println(F("EV_TXSTART"));
            break;
        case EV_TXCANCELED:
            Serial.println(F("EV_TXCANCELED"));
            break;
        case EV_RXSTART:
            /* do not print anything -- it wrecks timing */
            break;
        case EV_JOIN_TXCOMPLETE:
            Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
            break;

        default:
            Serial.print(F("Unknown event: "));
            Serial.println((unsigned) ev);
            break;
    }
}

void doSensorTask (void *parameter) {

    

    while(true) {
        if (isBME680) {
            while (! sd.readDataFromBME680())
                delay(500);
            delay(3020);
        } else {
            while (! sd.readData())
                delay(500);
        }
        xSemaphoreTake(semaphore, portMAX_DELAY);
        dataString = sd.getLoraString();
        xSemaphoreGive(semaphore);
    }
    
}

void do_send(osjob_t* j){
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {

        // dataString is written in the other thread/task
        xSemaphoreTake(semaphore, portMAX_DELAY);
        copyDataString(dataString);
        xSemaphoreGive(semaphore);

        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void convertAndCopyKeys(String loraAppKey,String loraDevEUI) {

    // copy DEVEUI
    char devEUIHex[loraDevEUI.length()+1];
    loraDevEUI.toCharArray(devEUIHex, loraDevEUI.length()+1);
    byte devEUIBytes[8];
    unHex(devEUIHex, devEUIBytes, strlen(devEUIHex));
    memcpy(DEVEUI, devEUIBytes, 8);

    // copy APPKEY
    char appKeyHex[loraAppKey.length()+1];
    loraAppKey.toCharArray(appKeyHex, loraAppKey.length()+1);
    byte appKeyBytes[16];
    unHex(appKeyHex, appKeyBytes, strlen(appKeyHex));
    memcpy(APPKEY, appKeyBytes, 16);

}

void copyDataString(String dataString) {
    int strleng = dataString.length();
    memcpy(mydata, dataString.c_str(), strleng);
}


void setup() {

    Serial.begin(115200);
    delay(4000);
    NVS.begin(); // Non-Volatile-Storage manager
    SPI.begin(5, 19, 27);

    #ifdef VCC_ENABLE
    // For Pinoccio Scout boards
    pinMode(VCC_ENABLE, OUTPUT);
    digitalWrite(VCC_ENABLE, HIGH);
    delay(1000);
    #endif

    //get mutex semaphore from OS
    semaphore = xSemaphoreCreateMutex();

    SensorSetup sensor;

    if(sensor.isFirstStart()) {

        sensor.setAP(AP_SSID, AP_PASS);
        // start setup phase for SETUP_PHASE_SECS seconds
        sensor.start(SETUP_PHASE_SECS, false, true);
        Serial.println("First start, please configure loraSensor via WiFi.");
        //printConfigParameter();
        ESP.restart();

    } else {

        // if you want to reconfigure run 'python esptool.py erase_flash'
        Serial.println("not the first start ... continue ...");

        // initialize sensors
        if (isBME680) {
            Serial.println("Using the Bosch BME680 Sensor.");
            // mode 1 -> measurement every 3 second possible (default)
            // mode 2 -> measurement after 300s possible
            sd.initBME680();
        } else {
            Serial.println("Using the CCS811 Sensor.");
            // mode 1 -> measurement every second possible (default)
            // mode 2 -> measurement every 10 second possible
            // mode 3 -> measurement every 60 second possible
            sd.init();
        }

        // warmup
        int warmupTime = WARMUP_TIME * 60 * 1000;
        Serial.printf("Starting warmup time for %d minutes.\n\n", WARMUP_TIME);
        while (warmupTime > millis()) {
            if (isBME680) {
                delay(3000);
                while(! sd.readDataFromBME680())
                    delay(500);
            } else {
                while(! sd.readData())
                    delay(500);         
            }
        }

        dataString = sd.getLoraString();

        // start sensor task on esp32 core 0 (the other one)
        Serial.println("Starting sensor Task on Core 0");
        delay(1000);
        xTaskCreatePinnedToCore(
            doSensorTask,
            "sensorTask",
            10000,
            NULL,
            1,
            &sensorTask,
            0);


        // load config parameter from flash
        roomID = NVS.getString(K_ROOM_ID);
        loraAppKey = NVS.getString(K_LORA_APPKEY);
        loraDevEUI = NVS.getString(K_LORA_DEVEUI);

        convertAndCopyKeys(loraAppKey, loraDevEUI);

        // LMIC init
        Serial.println("LMIC INIT...");
        os_init();
        // Reset the MAC state. Session and pending data transfers will be discarded.
        Serial.println("LMIC RESET ... ");
        LMIC_reset();

        // Start job (sending automatically starts OTAA too)
        do_send(&sendjob);
    }
}

void loop() {
    os_runloop_once();
}
