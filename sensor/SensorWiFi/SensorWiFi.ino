#define SCD30  false

#include<SensorSetup.h>
#include<SensorCom.h>
#include<SensorData.h>
#include "time.h"

/**** local configuration ****/

// wifi credentials for setup phase
const char AP_SSID[] = "sensor-wifi";
const char AP_PASS[] = "password";
#define SETUP_PHASE_SECS    240

/*****************************/

SensorCom com;
SensorData sd;
String roomID;
struct tm timeinfo;
String wifiSSID;
String wifiPass;

/*****************************/


void printConfigParameter(){

    Serial.print("\nRoomID: ");
    Serial.println(NVS.getString(K_ROOM_ID));
    Serial.print("\nRoomName: ");
    Serial.println(NVS.getString(K_ROOM_NAME));
    Serial.print("\nCA-Cert:\n");
    Serial.println(NVS.getString(K_CACERT));
    Serial.print("\nSensor-Cert:\n");
    Serial.println(NVS.getString(K_CERT));
    Serial.print("\nSensor-Key:\n");
    Serial.println(NVS.getString(K_KEY));
    Serial.print("\nSSID: ");
    Serial.println(NVS.getString(K_WIFI_SSID));
    Serial.print("\nWifi-Pass: ");
    Serial.println(NVS.getString(K_WIFI_PASS));
    Serial.print("\nEAP-User: ");
    Serial.println(NVS.getString(K_EAP_USER));
    Serial.print("\nEAP-Pass: ");
    Serial.println(NVS.getString(K_EAP_PASS));
    Serial.print("\nURL: ");
    Serial.println(NVS.getString(K_DATA_URL));
    Serial.print("\nHostname: ");
    Serial.println(NVS.getString(K_HOSTNAME));
}

int currTime() {
    time_t now;
    time(&now);
    return now;              
}

void showHumanReadableTime() {
    
    if(!getLocalTime(&timeinfo)){
        Serial.println("\nFailed to obtain time");
        return;
    }
    Serial.println(&timeinfo, "\n%A, %B %d %Y %H:%M:%S");
}

void wifiReconnect() {
    // TODO currently without EAP
    
    WiFi.begin(wifiSSID.c_str(), wifiPass.c_str());
    Serial.println("Try to connect to WiFi (PSK-Mode)");
    
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(1000);
    }
}


/******** SETUP ************/

void setup() {

    Serial.begin(115200);
    delay(4000);
    NVS.begin(); // Non-Volatile-Storage manager
    SensorSetup sensor;

    if(sensor.isFirstStart()) {

        sensor.setAP(AP_SSID, AP_PASS);
        // start setup phase for SETUP_PHASE_SECS seconds
        sensor.start(SETUP_PHASE_SECS, false, false);
        Serial.println("First start, please configure wifiSensor via WiFi.");
        //printConfigParameter();
        ESP.restart();
        
    } else {
      
        // if you want to reconfigure run 'python esptool.py erase_flash'
        Serial.println("not the first start ... continue ...");
        
        // get wifi credentials from flash and connect
        wifiSSID = NVS.getString(K_WIFI_SSID);
        wifiPass = NVS.getString(K_WIFI_PASS);        
        
        // if there is no wifi pass, then EAP is used to establish a connection
        if (wifiPass != NULL && wifiPass.length() > 1) { 
            Serial.println("WiFi: PSK-MODE");
            wifiReconnect();    
        } else {
            // TODO
             Serial.println("WiFi: EAP-MODE");
        }


        roomID = NVS.getString(K_ROOM_ID);


            
        // set time via NTP
        configTime(3600, 3600, "ptbtime3.ptb.de");
        showHumanReadableTime();
        // initialize TLS-Client
        com.setServer();


        // initialize sensors
        sd.init(1);
    }
}
/******** LOOP ************/

unsigned long nextDuration = 20 * 60 * 1000; // 20 minutes warm up time due to datasheet
unsigned long previousTime = 0;

void loop() {
    
    unsigned long currentTime = millis();

    sd.readData();
    //sd.printData();

    if (WiFi.status() != WL_CONNECTED){wifiReconnect();}
    
    if (currentTime - previousTime >= nextDuration) {
        previousTime = currentTime;
        String jsonPayload = sd.getJson(roomID);
        Serial.println(jsonPayload);
        com.sendPost(jsonPayload);
        nextDuration = 60 * 1000;  // one minute
    }
}
