# Sensor

Im `libraries`-Verzeichnis befinden sich die Module die von allen beiden Sensoren und
dem BLEAdvertiser genutzt werden. Folgende Grafik zwigt die Modulabhängigkeiten
für die einzeönen Sketches:

![Module](doc/sensor_modules.png)

## SensorWiFi

[SensorWiFi.ino](/sensor/SensorWiFi/SensorWiFi.ino) ist der Sketch, der auf
per WiFi angebundene Mikrocontroller geladen wird.

## SensorLoRa

[SensorLora.ino](/sensor/SensorLora/SensorLora.ino) ist der Sketch, der auf
per LoRaWAN angebundene Mikrocontroller geladen wird.

Um den Sketch mithilfe von ArduinoStudio 1.8.13 auf den Mikrocontroller zu laden sind
zuvor die Schritte unter [BSEC-Arduino-Readme](https://github.com/BoschSensortec/BSEC-Arduino-library)
durchzuführen.

## BLEAdvertiser

[BLEAdvertiser.ino](/sensor/BLEAdvertiser/BLEAdvertiser.ino) ist der Sketch, der auf
per LoRaWAN angebundene Mikrocontroller geladen wird.

## Externe Abhängigkeiten

[NVS](https://github.com/rpolitex/ArduinoNvs)

[DHT Sensor Library](https://github.com/adafruit/DHT-sensor-library)

[Adafruit CCS811 Library](https://github.com/adafruit/Adafruit_CCS811)

[Arduino-LMIC library (LoRaWAN)](https://github.com/mcci-catena/arduino-lmic)

[BSEC-Arduino-library (BME680 nur für LoRa-Sensor)](https://github.com/BoschSensortec/BSEC-Arduino-library)

[Bibliothek für SCD30 CO2-Sensor](https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library)



Alle anderen verwendeten Bibliotheken sind in
[arduino-esp32](https://github.com/espressif/arduino-esp32) enthalten,
dem Arduino kompatiblen Framework des ESP32 Herstellers Espressif.

Für das Projekt wird die Version 1.0.5-rc4 verwendet. Folgende *Boardverwalter-URL*
muss in ArduinoStidio eingetrgen werden um diese Version zu beziehen.

```
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_dev_index.json
```


## Inbetriebnahme

###  Voraussetzungen

Es wird empfohlen den Sketches mithilfe
von ArduinoStudio auf den Mikrocontroller zu laden. In ArduinoStudio sind vor dem
Aufspielen folgende Einstellungen zu tätigen:

1. Das Partitionsschema (hier am Beispiel eines ESP32 mit 4MB Flash) muss auf
`NO OTA (2MB APP/2MB SPIFFS)` geändert werden.

2. Die externe Abhängigkeit muss vor dem Übersetzen installiert sein. Hier am Beispiel
von Linux oder MAC gezeigt:

```bash
$ cd ~/Arduino/libraries/
$ git clone --depth=1 https://github.com/rpolitex/ArduinoNvs

```

Alle anderen Abhängigkeiten können über den Bibliotheksveralter in ArduinoStudio
installiert werden.


3. Es muss eine funktionierende Konfiguration auf den Sensor aufgebracht werden. Dazu
sollte `sensor_set_tool` verwendet werden. Die Funktionsweise wird im nächsten
Abschnitt erläutert. **Alle Sensortypen und der BLEAdvertiser können mit diesem
Werkezeug konfiguriert werden.**

4. Es wird eine funktionierende Installation von *openssl* im Standardpfad vorausgesetzt.


### Konfiguration Erstellen und Übertragen mit `sensor_setup_tool`

Um eine Konfiguration für den Sensor zu erstellen und zu übertragen verwendet man
das Tool `sensor_setup_tool`. Es ist in der Lage eine CA und Zertifikate zu erstellen
und zu verwalten. Die Zertifikate können im Anschluss, inlusive weiteren
Konfigurationsparametern auf den Server übertragen werden. Zudem kann `sensor_setup_tool`
auch Zertifikate widerrufen und eine CRL erstellen.

Im folgenden werden die essentiellen Schritte zur Inbetriebahme eines Sensors
beschrieben

#### 1. CA

Mit dem folgenden Befehl kann man im aktuellen Verzeichnis eine neue CA erstellen,
dabei werden einige Dateien und Verzeichnise erstellt, diese Dateien dürfen
nicht manuell bearbeitet werden.

```bash
$ python sensor_setup_tool -n -c co2-ca
```

Nach dem Aufruf verlangt *openssl* die Eingabe eines Passwortes, dass den
privaten Schlüssel der CA verschlüsselt (mittels AES256). Diesen Passwort ist
unbedingt sicher zu Verwahren, ebenso sollte der Ort an dem die CA erstellt wird
als Vertreuenswürdig gelten.

#### 2. Ein neues Client-Zertifikat erstellen und signieren

Client-Zertifikate können sowohl auf einem Sensor als auch für den THC-Server-Proxy
verwendet werden.


```bash
$ python sensor_setup_tool -a -c sensor.example.com
```

nach dem Aufruf verlangt *openssl* das CA-Password um das neue Zertifikat zu signieren.
Das Zertifikat wurde in die Datei `cert/sensor.example.com-cert.pem` geschrieben. Der
Schlüssel liegt in der Datei `private/sensor.example.com-key.pem`. Zertifikat können
nun auf den Server kopiert und verwendet werden.


```bash
$ python sensor_setup_tool -a -c sensor001
```

Das Erstellen von Sensor-Zertfifikaten unterscheidet sich nicht.


##### data.csv

Um eine Übersicht über alle erstellten Zertifikate und relevanten Konfigurationsparameter
zu ermöglichen, loggt `sensor_setup_tool` alle relevanten Konfigurationsparameter
in die Datei `data.csv`. Folgende Parameter (Spalten) werden gespeichert und können
so bequem durch andere Programme  eingelesen werden:


* `serial` -> Seriennummer des Zertifikates

* `name` -> der **C**ommon**N**ame, wird bei jedem Kommando über Option `-c` übergeben

* `roomid` -> UUID zur eindeutigen identifizierung eines Raumes/Sensors

* `fingerprint_sha256` -> SHA256 Hashsumme des Zertifikates
kann zudem mittels `./sensor_setup_tool -f -c CN` bgefragt werden

* `revoked` -> [True | False] True: Zertifikat wurde widerufen

#### 3. Übertragen der Konfiguration auf den Mikrocontroller

Beim ersten Start des Sensors wird für **zwei** Minuten die Setup-Phase gestartet. Zu
diesem Zweck wird ein Access Point mit folgenden Daten erstellt, mit diesem muss man
sich verbinden:

**WiFi**: 802.11n

**SSID**: `sensor-wifi`

**PASSWORD**: `password`

Die Verbindung zum Sensor erfolgt über IPv4, die Adresse des Sensors lautet:
`10.0.0.254/24`. Man muss also eine IPv4 Adresse aus diesem Subnetz verwenden.

Im Anschluss kann man die Daten auf den Sensor übertragen:

```bash
$ python sensor_setup_tool -s -c sensor001

```

`sensor_setup_tool` liest zusätzliche, veränderliche Konfigurationsdaten aus der
Konfigurationsdatei `config/default.conf`:


```ini
[general]
data_url = https://sensor.nachtsieb.de/api/sensor/measurement/store

[wifi]
ssid = funk_mit_mir_gast
pass = SECRET

[eap]
user = some_user
pass = SOME_SECRET
```

Nach einem Neustart des Sensors ist dieser Einsatzbereit.


#### 4. Widerruf eines Zertifikates und Erstellen einer CRL

Zertifikate kann man mithilfe der Option `-r` widerrufen.


```bash
$ python sensor_setup_tool -r -c sensor_bad

```

Nach dem Aufruf verlangt *openssl* zweimal die Eingabe des CA-Passwortes,
für den Rückruf und das Erstellen der **C**ertificate **R**evocation **L**ist
`co2-crl.pem`. Diese CRL kann dann zur Filterung von kompromitierten oder
defekten Sensoren eingesetzt werden.

#### 5. Erstellen eines QR-Codes

Die erstellten QR-Codes befinden sich im Verzeichnis `qrcodes`.


```bash
$ python sensor_setup_tool -q -c sensor0815

```

Beispiel eines erstellen QR-Codes

![QR-Code](doc/qrcode.png)

#### 6. Konfiguration auf BLE-Advertiser übertragen


```bash
$ python sensor_setup_tool -b -c sensor0815

```

#### 7. Inbetriebnahme eines LoRaWAN-Sensors

##### 7.1 RoomID und Zertifikat erstellen

Bevor eine LoRaWAN-Konfiguration übertragen wird, muss ein gültiges Zertifikat (siehe 2.)
erstellt werden, auch wenn das Zertfifikat nicht genutzt wird.


```bash
$ python sensor_setup_tool -a -c lora001
```

##### 7.2 Registrieren des Gerätes bei The Things Network

1. DeviceID = RoomID / Raumname (UUID mit der Endung c02caffebabe)
Diese Information wird im vorigen Schritt generiert und in die Dateie *data.csv* geschrieben


```
grep lora001 data.csv | cut -d "," -f 4
```

2. DeviceEUI erstellen -> Wird für den nächsten Schritt benötigt

3. AppKey wird durch TTN erstellt -> Wird für den nächsten Schritt benötigt


##### 7.3 Übertragen der Konfiguration auf den Sensor

```
./sensor_setup_tool -l -t DEVEUI,APPKEY -c lora001

```

Beispiel:


```
./sensor_setup_tool -l -t C02CAFFEBABE4711,65F323EFF4AF76E7FA7590FF7A913D8E -c lora001

```



## Verkabelung


![Verkabelung](doc/Verkabelung.png)


ESP32(3V3) -> DHT22(+/PIN 1), CCS811(3V3)

ESP32(GPIO 4) -> DHT22(out/PIN 2, also connect a 10k Ohm pull-up resistor)

ESP32(GND) -> DHT22(-/PIN3)

ESP32(GND) -> CCS811(GND)

ESP32(GPIO 21) -> CCS811(SDA)

ESP32(GPIO 22) -> CCS811(SCL)

ESP32(GND) -> CCS811(WAKE)

Die drei Vorkommen von ESP32(GND) sind jeweils verschiedene GND-Pins am ESP32.


## TODO:

1. [x] Modul für das Deployment des Sensors: [SensorSetup-Modul](/sensor/libraries/SensorSetup)
2. [x] Modul für die Authentifizierung und Versenden der Messergbnisse:[SensorCom-Modul](/sensor/libraries/SensorCom)
3. [x] Modul für das Versenden von BLE-Beacons: [SensorBLE-Modul](/sensor/libraries/SensorBLE)
4. [x] Modul für Formattierung und Aufbereitung der Messergebnisse:[SensorData-Modul](/sensor/libraries/SensorData)


### sensor\_setup\_tool

1. [x] Server-Konfigurationsparamter
2. [ ] Löschen von Zertifikaten
