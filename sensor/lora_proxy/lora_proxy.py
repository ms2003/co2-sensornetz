#!/usr/bin/env python3
"""Script which acts as a proxy between The Things Network and the THC-Server.

Therefore it  fetches data from The Things Network DB integration, does a
reformating and send it to the THC-Sensor API.

Written for the co2-sensor project.

WARNING: JUST A PROTOTYPE SERVICE WITHOUT PROPER HANDLING OF START AND END!!!

VERSION = 0.1.0

"""

import json
import sys
import time
from datetime import datetime
from pathlib import Path

import requests

BASE_PATH = "/security/lora_proxy"  # path in home directory

CA_CERT = BASE_PATH + "/co2-ca-cert.pem"
CLIENT_CERT = BASE_PATH + "/lora_proxy-cert.pem"
CLIENT_KEY = BASE_PATH + "/lora_proxy-key.pem"

TTN_ACCESS_KEY_FILE = BASE_PATH + "/lora_proxy_access_key"
TTN_BASE_URL = "https://co2caffebabe.data.thethingsnetwork.org/api/v2/"

QUERY_TIME = 61  # the time beetween queries in seconds


def error_exit(err_message, exit_code=1, exit=True):
    """Print error message and exit script."""
    sys.stderr.write(f"{err_message}\n")
    if exit:
        sys.exit(1)


def check_certs(context):
    path_map = {"cacert": CA_CERT, "cert": CLIENT_CERT, "key": CLIENT_KEY}

    for key, value in path_map.items():
        path = Path(str(Path.home()) + value)
        if path.is_file():
            context[key] = str(path)
        else:
            error_exit(f"File {str(path)} does not exist.")


def load_ttn_access_key():
    """Load and return the access key for the TTN integration Database."""
    key_path = Path(str(Path.home()) + TTN_ACCESS_KEY_FILE)

    if key_path.is_file():
        return key_path.read_text().strip()
    else:
        error_exit(f"{TTN_ACCESS_KEY_FILE} does not exists")


def get_values_for_all_sensor(context, time_seconds=QUERY_TIME):
    """Request the last value for aspecific sensor."""
    query_url = TTN_BASE_URL + "query"
    headers = {
        "Authorization": f"key {context['ttn_key']}",
        "Accept": "application/json",
    }
    param = {"last": f"{str(time_seconds)}s"}

    r = requests.get(query_url, headers=headers, params=param)

    if not r.status_code == 200:
        error_exit(
            f"Problem requesting {query_url} - Status Code: {r.status_code}",
            exit=False,
        )
    try:
        json_string = r.json()
    except Exception:
        error_exit(f"Unable to decode json response from {query_url}", exit=False)
        return

    # build a list of objects which can later be serialized and send to thc
    return convert_values(json_string)


def convert_values(json_string):
    """Convert the json response from ttn to a list of objects thc understands."""
    if json_string is None:
        return

    obj_list = []
    for entry in json_string:
        values = entry["value"].split(";")
        obj = {
            "roomID": entry["device_id"],
            # "timestamp": get_epoch_time(entry["time"]),
            "temperature": values[0],
            "humidity": values[1],
            "co2concentration": values[2],
            "tvoc": values[3],
            # "pressure": values[6],
        }
        obj_list.append(obj)

    return obj_list


def get_epoch_time(ttn_date_string):
    """Return the given iso like date string from ttn to epoch time.

    The String must be altered first due to much precision for the python3
    datetime module.
    """
    sanitized_date_string = ttn_date_string.split(".")[0] + "Z"
    dt = datetime.strptime(sanitized_date_string, "%Y-%m-%dT%H:%M:%S%z")
    return dt.strftime("%s")


def send_values_to_thc(context, sensor_values):
    """Send values to the THC-Server."""
    if sensor_values is None:
        return

    query_url = "https://sensor.nachtsieb.de/api/sensor/measurement/store"
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    for value in sensor_values:
        json_val = json.dumps(value)
        # print(json_val)

        r = requests.post(
            query_url,
            headers=headers,
            verify=context["cacert"],
            cert=(context["cert"], context["key"]),
            data=json_val,
        )

        if not r.status_code == 200:
            sys.stderr.write(r.text)
            error_exit(f"Unable to request ressource {query_url}", exit=False)


def main():
    """Start the proxy."""
    context = {}
    context["ttn_key"] = load_ttn_access_key()
    check_certs(context)

    while True:
        sensor_values = get_values_for_all_sensor(context)
        send_values_to_thc(context, sensor_values)
        time.sleep(QUERY_TIME)


main()
