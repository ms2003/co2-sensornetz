/*
 * SensorData.h - Library for reading measurements from the DHT22 and the CCS811.
 *
 * Created by Martin Steinbach, December, 2020.
 *
 * See file LICENSE for license information.
 */

#include "SensorData.h"
#include <esp32-hal-log.h>

DHT dht(DHTPIN, DHTTYPE);
Adafruit_CCS811 ccs;
Bsec iaqSensor;

void SensorData::initBME680(uint8_t bmeMode) {

    _dp.timestamp  = 0;
    _dp.humid      = 0;
    _dp.temp       = 0;
    _dp.co2        = 0;
    _dp.tvoc       = 0;

    Wire.begin();

    iaqSensor.begin(BME680_I2C_ADDR_SECONDARY, Wire);

    String output =  "\nBSEC library version "
              + String(iaqSensor.version.major)
              + "." + String(iaqSensor.version.minor)
              + "." + String(iaqSensor.version.major_bugfix)
              + "." + String(iaqSensor.version.minor_bugfix);
    Serial.println(output);

    bsec_virtual_sensor_t sensorList[6] = {
        BSEC_OUTPUT_RAW_PRESSURE,
        BSEC_OUTPUT_STATIC_IAQ,
        BSEC_OUTPUT_CO2_EQUIVALENT,
        BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
        BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
        BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
    };

    if (bmeMode == 1) {
        // data after 3 Seconds available
        iaqSensor.updateSubscription(sensorList, 6, BSEC_SAMPLE_RATE_LP);
    } else {
        // data after 300 Seconds available
        iaqSensor.updateSubscription(sensorList, 6, BSEC_SAMPLE_RATE_ULP);
    }


}

bool SensorData::readDataFromBME680() {

    float pressure = 0;

    uint8_t staticIaqAccuracy = 0;

    if (iaqSensor.run()){ // if new measurements available

        _dp.temp        = iaqSensor.temperature;
        _dp.humid       = iaqSensor.humidity;
        _dp.co2         = iaqSensor.co2Equivalent;
        _dp.tvoc        = iaqSensor.breathVocEquivalent;

        pressure        = iaqSensor.pressure / 100;

        staticIaqAccuracy   = iaqSensor.staticIaqAccuracy;

        log_d("Accuracy: STATIC-IAQ: %d",staticIaqAccuracy);
        log_d("TEMP: %f - HUMID: %f - CO2: %d - PRESS: %f - staticIAQ: %f bVOC: %f\n",
                _dp.temp, _dp.humid, _dp.co2, pressure, iaqSensor.staticIaq ,_dp.tvoc);

        return true;
    } else {
        return false;
    }
}
#if (SCD30)
SCD30 scd30;
void SensorData::initSCD30(bool autoSelfCalibration) {

    _dp.timestamp  = 0;
    _dp.humid      = 0;
    _dp.temp       = 0;
    _dp.co2        = 0;
    _dp.tvoc       = 0;

    Wire.begin();


    // Note: The SCD30 has an automatic self-calibration routine.
    // Sensirion recommends 7 days of continuous readings with at least 1 hour a day of 'fresh air' for self-calibration to complete.
    if (!scd30.begin(autoSelfCalibration))
    {
        log_e("unable to detect SCD30");
        return;
    }

    if(!autoSelfCalibration){
        scd30.setMeasurementInterval(30);
    }
    else{
        scd30.setMeasurementInterval(2);
    }

    if (scd30.getTemperatureOffset() != 1)
        scd30.setTemperatureOffset(1);

}

bool SensorData::readDataFromSCD30() {
    //The SCD30 has data ready every two seconds (default)

    _dp.temp = scd30.getTemperature();
    _dp.humid = scd30.getHumidity();

    if (isnan(_dp.humid) || isnan(_dp.temp))
    {
        return false;
    }

    _dp.co2 = scd30.getCO2();

    log_d("TEMP: %f - HUMID: %f - CO2: %d - TVOC: %d\n",
          _dp.temp, _dp.humid, _dp.co2, _dp.tvoc);

    return true;
}
#endif

void SensorData::init(uint8_t ccsMode) {

    _dp.timestamp  = 0;
    _dp.humid      = 0;
    _dp.temp       = 0;
    _dp.co2        = 0;
    _dp.tvoc       = 0;

    // start dht22
    dht.begin();

    if(!ccs.begin()){
        log_e("unable to start CCS811");
        return;
    }

    // the ccs811 should messure every second
    if (ccsMode == 1)
        ccs.setDriveMode(CCS811_DRIVE_MODE_1SEC);
    else if (ccsMode = 2)
        ccs.setDriveMode(CCS811_DRIVE_MODE_10SEC);
    else
        ccs.setDriveMode(CCS811_DRIVE_MODE_60SEC);

    // calibrate sensor
    while(!ccs.available());
    float ccs_temp = ccs.calculateTemperature();
    ccs.setTempOffset(ccs_temp - 25);

}

bool SensorData::readData(uint8_t rounds) {

    uint8_t actRounds = 0;
    uint8_t discardedValues;


    // drop the firt 50% of the measured values if more than 2 rounds are requested
    if (rounds > 1 )
        discardedValues  = rounds - (rounds / 2);
    else
        discardedValues  = 0;

    /***** read temperature and humidity *****/

    _dp.temp = dht.readTemperature();
    _dp.humid  = dht.readHumidity();
    if (isnan(_dp.humid) || isnan(_dp.temp)) {return false;}

    /***** co2 concentration *****/

    // set environmental data for better results
    ccs.setEnvironmentalData(_dp.humid,_dp.temp);

    _dp.co2 = 0;
    _dp.tvoc = 0;

    for (uint8_t i = 0; i < rounds ; i++) {

        // needed otherwise the sensor will never be available again
        ccs.calculateTemperature();

        delay(1000);

        while(!ccs.available());

        if(ccs.readData() && i >= discardedValues) {
            _dp.co2     += ccs.geteCO2();
            _dp.tvoc    += ccs.getTVOC();
            actRounds++;
        }

    }

    if (actRounds == 0) {
        log_d("no measurement was taken");
        return false;
    }


    _dp.co2  = _dp.co2 / actRounds;
    _dp.tvoc = _dp.tvoc / actRounds;

    log_d("After %d rounds:", actRounds);
    log_d("TEMP: %f - HUMID: %f - CO2: %d - TVOC: %d\n",
                _dp.temp, _dp.humid, _dp.co2, _dp.tvoc);

    return true;
}

void SensorData::printData() {
    Serial.printf(
            "Temperature: %.2f Humidity: %.2f CO2: %d TVOC: %d\n",
            _dp.temp, _dp.humid, _dp.co2, _dp.tvoc);
}

String SensorData::getJson(String roomID) {

    String json = "{\"roomID\":\"" + roomID + "\",\"temperature\":" + _dp.temp
        + ",\"humidity\":" + _dp.humid + ",\"co2concentration\":" + _dp.co2
        + ",\"tvoc\":" + _dp.tvoc + "}";

    return json;
}

String SensorData::getJsonNoTvoc(String roomID) {

    String json = "{\"roomID\":\"" + roomID + "\",\"temperature\":" + _dp.temp
        + ",\"humidity\":" + _dp.humid + ",\"co2concentration\":" + _dp.co2
        + "}";

    return json;
}

String SensorData::getLoraString() {

    String str =    String(_dp.temp)    + ";"
                +   String(_dp.humid)   + ";"
                +   String(_dp.co2)     + ";"
                +   String(_dp.tvoc);

    return str;
}
