/*
 * SensorData.h - Library for reading measurements from the DHT22 and the CCS811.
 *
 * Created by Martin Steinbach, December, 2020.
 *
 * See file LICENSE for license information.
 */

#ifndef SensorData_h
#define SensorData_h
#include "Arduino.h"
#include <DHT.h>
#include <Adafruit_CCS811.h>

#if (SCD30)
#include <SparkFun_SCD30_Arduino_Library.h>
#endif

// bosch library for bme680
// instruction for using with arduino studio:
// https://github.com/BoschSensortec/BSEC-Arduino-library
#include "bsec.h"

#define DHTTYPE DHT22
#define DHTPIN 4

typedef struct {
    uint32_t    timestamp;
    float       humid;
    float       temp;
    uint32_t    co2;
    uint32_t    tvoc;
}DataPoint;

class SensorData {
    public:
        void init(uint8_t ccsMode = 1);
        void initBME680(uint8_t bmeMode = 1);
        void initSCD30(bool autoSelfCalibration = false);
        bool readData(uint8_t rounds = 1);
        bool readDataFromBME680();
        bool readDataFromSCD30();
        void printData();
        String getJson(String roomID);
        String getJsonNoTvoc(String roomID);
        String getLoraString();
   private:
        DataPoint   _dp;
};

#endif
