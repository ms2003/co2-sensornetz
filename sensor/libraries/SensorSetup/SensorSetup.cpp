/*
 * SensorSetup.cpp - Library for managing the setup phase of the sensor.
 *
 * Created by Martin Steinbach, December, 2020.
 *
 * See file LICENSE for license information.
 */

#include "SensorSetup.h"
#include <esp32-hal-log.h>
extern "C" {
#include "crypto/base64.h"
}

WebServer server(80);
ConfigCheck     _cc;

/*
 * Uptime is returned in seconds since start
 */
unsigned int SensorSetup::getUptime() {
    return (unsigned int)esp_timer_get_time() / 1000 / 1000;
}
/*
 * Takes a pair of strings k,v and write them presistent to flash
 */
static void writeToFlash(String key, String value) {
        NVS.setString(key, value);
}

static String getFromFlash(String key) {
        return NVS.getString(key);
}

/*
 * Returns a String where the blanks are decoded to '+'.
 * Offset is the chars that where ignored at beginning and
 * the end of the source string,
 */
static String correctURLEncoding(String base64Fail, int offset) {
    String base64 = base64Fail;

    for (int i = offset; i < base64Fail.length() - offset; i++) {
        if (base64[i] == ' ') {base64[i] = '+';}
    }

    return base64;
}

static String decodeBase64(String base64) {
    size_t outputLen;
    base64 = correctURLEncoding(base64,0);

    unsigned char * decoded = base64_decode(
            (const unsigned char *)base64.c_str(),
            base64.length(),
            &outputLen
            );

    String dec_string = "";

    for(int i = 0 ; i < outputLen ; i++) {
        dec_string.concat((char)decoded[i]);
    }

    return dec_string;
}

/*
 * Returns true if the the given string looks like a uuid
 * e.g. (aa7e54a9-8099-4ee0-84f8-441d3821a214)
 */
static bool isUUID(String uuid) {
    // normaly i would use regex but in fact it is a microcontroller ..
    //
    if (uuid == NULL) {
        log_d("uuid is NULL");
        return false;
    }

    int length = uuid.length();
    log_d("check uuid");

    if (length != 36) {
        log_d("uuid invalid: length");
        return false;
    }

    uuid.toLowerCase();

    for (int i = 0; i < 36 ; i++) {
        if (i == 8 || i == 13 || i== 18 || i== 23) {
            if (uuid[i] != '-') {return false;}
        } else if (uuid[i] < '0' && uuid[i] > 'f'){
            return false;
        }
    }
        return true;
}

/*
 * Returns true if all config parameter where transmitted and written to flash
 */
static bool areAllConfigsSet(bool isBLEAdvertiser, bool isLora) {

    // a BLE-Advertiser just needs the roomID
    if (isBLEAdvertiser) {
        
        return _cc.isRoomIdSet;
        
    } else if (isLora) {
        
        if(! _cc.isRoomIdSet)   {log_w("RoomID not set"); return false;}
        if(! _cc.isAppKeySet)   {log_w("RoomID not set"); return false;}
        if(! _cc.isDevEuiSet)   {log_w("RoomID not set"); return false;}
        
    } else{  

        if(! _cc.isRoomIdSet)   {log_w("RoomID not set"); return false;}
        if(! _cc.isRoomNameSet) {log_w("RoomName not set");return false;}
        if(! _cc.isCaCertSet)   {log_w("CA_CERT not set");return false;}
        if(! _cc.isCertSet)     {log_w("CERT not set");return false;}
        if(! _cc.isKeySet)      {log_w("KEY not set");return false;}
        if(! _cc.isWifiSet)     {log_w("WiFi not set");return false;}
        if(! _cc.isDataURLSet)  {log_w("URL not set");return false;}
        
    }

    return true;
}

/*
 * Sends HTTP status code 400 (bad request) to sender
 */
static void sendErrorResponse(String argName) {
        server.send(400, "text/plain", "argument " + argName + " has invalid value");
}

/*
 * Sends HTTP status code 200 (OK) to sender
 */
static void sendOK() {
        server.send(200);
}

/*
 * The next methods apply some simple checks on the config parameters
 */
static void processRoomID(String uuid) {
    log_d("processing %s", K_ROOM_ID);
        if (isUUID(uuid)){
            writeToFlash(K_ROOM_ID, uuid);
            _cc.isRoomIdSet = true;
            sendOK();
        } else {
            sendErrorResponse(K_ROOM_ID);
        }
}

static void processRoomName(String roomName) {
    log_d("processing %s", K_ROOM_NAME);
        if (roomName.length() > 0){
            writeToFlash(K_ROOM_NAME, roomName);
            _cc.isRoomNameSet = true;
            sendOK();
        } else {
            sendErrorResponse(K_ROOM_NAME);
        }
}

static void processWifi(String key, String arg) {
    log_d("processing %s", key);
    arg = decodeBase64(arg);
        if (arg.length() > 0 && arg.length() < 64){
            writeToFlash(key, arg);
            if(key.equals(K_WIFI_SSID)) {_cc.isWifiSet = true;}
            sendOK();
        } else {
            sendErrorResponse(key);
        }
}

static void processPEM(String key, String arg) {
    log_d("processing %s", K_KEY);
        if (arg.startsWith("-----BEGIN ")){
        arg = correctURLEncoding(arg, 25);
            writeToFlash(key, arg);
            if (key.equals(K_KEY))      {_cc.isKeySet = true;};
            if (key.equals(K_CACERT))   {_cc.isCaCertSet = true;}
            if (key.equals(K_CERT))     {_cc.isCertSet = true;}
            sendOK();
        } else {
            sendErrorResponse(key);
        }
}


static void processEAP(String key, String arg) {
    log_d("processing %s", key);
    arg = decodeBase64(arg);
        if (arg.length() > 3 && arg.length() < 64){
            writeToFlash(key, arg);
            sendOK();
        } else {
            sendErrorResponse(key);
        }
}


static void processURL(String key, String arg) {
    log_d("processing %s", key);
    arg = decodeBase64(arg);
        if (arg.startsWith("https://")){
            writeToFlash(key, arg);
            _cc.isDataURLSet = true;
            sendOK();
        } else {
            sendErrorResponse(key);
        }
}

static void processString(String key, String arg) {
    log_d("processing %s", key);
    arg = decodeBase64(arg);
        if (arg.length() > 3 && arg.length() < 64){
            writeToFlash(key, arg);
            if (key.equals(K_HOSTNAME))      {_cc.isHostnameSet = true;};
            sendOK();
        } else {
            sendErrorResponse(key);
        }
}

static void processLora(String key, String arg, uint8_t strLength) {
    log_d("processing %s", key);
        if (arg.length() == strLength){
            writeToFlash(key, arg);
            if (key.equals(K_LORA_APPKEY))      {_cc.isAppKeySet = true;};
            if (key.equals(K_LORA_DEVEUI))      {_cc.isDevEuiSet = true;};
            sendOK();
        } else {
            sendErrorResponse(key);
        }
}
/*
 * Method is trying to identify the mandatory configuration arguments from
 * the HTTP request and calls the appropriate method to process the value.
 */
static void handleBody() {

    /*
     * Body must contain a key=value pair. For each config option
         * a single request must be sent.
     */

    log_d("Argument count: %d", server.args());
    log_d("First argument name: %s", server.argName(0));

    // check if a argument is provided
    if (server.args() != 1){
        server.send(400, "text/plain", "no arguments provided");
    }

    // check if a correct argument is provided
    String arg = server.argName(0);
    if (arg.equals(K_ROOM_ID)){
        processRoomID(server.arg(arg));
    } else if (arg.equals(K_ROOM_NAME)) {
        processRoomName(server.arg(arg));
    } else if (arg.equals(K_CACERT) || arg.equals(K_CERT) || arg.equals(K_KEY)){
        processPEM(arg, server.arg(arg));
    } else if (arg.equals(K_WIFI_PASS) || arg.equals(K_WIFI_SSID)){
        processWifi(arg, server.arg(arg));
    } else if (arg.equals(K_EAP_USER) || arg.equals(K_EAP_PASS)){
        processEAP(arg, server.arg(arg));
    } else if (arg.equals(K_DATA_URL)){
        processURL(arg, server.arg(arg));
    } else if (arg.equals(K_HOSTNAME)){
        processString(arg, server.arg(arg));
    } else if (arg.equals(K_LORA_APPKEY)){
        processLora(arg, server.arg(arg), 32);
    } else if (arg.equals(K_LORA_DEVEUI)){
        processLora(arg, server.arg(arg), 16);
    } else {
        server.send(400, "text/plain", "arguments " + arg + " unknown");
    }
}

/*
 * Returns true if it is the first start of the sensor. That means no value
 * for the key K_ROOM_ID was found or the value is not a proper uuid.
 */
bool SensorSetup::isFirstStart() {
    String value = NVS.getString(K_ROOM_ID);
    log_d("First-Start-Check");
    return !isUUID(value);
}

/*
 * Set up and start a WiFi-AP, takes a ssid and a password.
 */
void SensorSetup::setAP(const char* ssid, const char* pass) {

    IPAddress localIP(10,0,0,254);
    IPAddress gateway(10,0,0,254);
    IPAddress subnet(255,255,255,0);

    // start WiFi-AP an channel 3
    WiFi.softAP(ssid, pass, 3);
    delay(1000);
    WiFi.softAPConfig(localIP, gateway, subnet);
    delay(3000);
    log_i("AP started");
}

/*
 * Starts the webserver to listen to incoming requests.
 */
bool SensorSetup::start(unsigned int seconds, bool isBLEAdvertiser, bool isLora){

    unsigned int currentUptime = 0;
    unsigned int startUptime = 0;
    unsigned int stopUptime = 0;

    // start and configure the web server
    server.on("/config",HTTP_POST, handleBody);
    server.begin();
    log_i("HTTP-Server started and listen on /config");

    startUptime = getUptime();
    stopUptime = currentUptime + seconds;

    while(stopUptime > currentUptime) {
        server.handleClient();
        currentUptime = getUptime();
    }

    if ( areAllConfigsSet(isBLEAdvertiser, isLora)) {
        log_i("all configuration parameter successful set");
    } else {
        log_w("not all config paramter transmitted or valid");
        log_d("erasing potentially %s", K_ROOM_ID);
        writeToFlash(K_ROOM_ID, "");
    }

    server.stop();
    WiFi.softAPdisconnect();
    log_i("Webserver and AP closed");
}
