/*
 * SensorSetup.h - Library for managing the setup phase of the sensor.
 *
 * Created by Martin Steinbach, December, 2020.
 *
 * See file LICENSE for license information.
 */

// NVS-keys
#define K_ROOM_ID       "ROOMID"
#define K_ROOM_NAME     "ROOMNAME"
#define K_CACERT        "CACERT"
#define K_CERT          "CERT"
#define K_KEY           "KEY"
#define K_WIFI_SSID     "WIFI_SSID"
#define K_WIFI_PASS     "WIFI_PASS"
#define K_EAP_USER      "EAP_USER"
#define K_EAP_PASS      "EAP_PASS"
#define K_DATA_URL      "DATA_URL"
#define K_HOSTNAME      "HOSTNAME"
#define K_UPDATE_URL    "UPDATE_URL"
#define K_LORA_APPKEY   "LORA_APPKEY"
#define K_LORA_DEVEUI   "LORA_DEVEUI"

#ifndef SensorSetup_h
#define SensorSetup_h
#include "Arduino.h"
#include "ArduinoNvs.h"
#include <WebServer.h>

typedef struct {
    bool isRoomIdSet=false;
    bool isRoomNameSet=false;
    bool isCaCertSet=false;
    bool isCertSet=false;
    bool isKeySet=false;
    bool isWifiSet=false;
    bool isDataURLSet=false;
    bool isHostnameSet=false;
    bool isAppKeySet=false;
    bool isDevEuiSet=false;
}ConfigCheck;

class SensorSetup {
    public:
        bool isFirstStart();
        void setAP(const char* ssid, const char* pass);
        bool start(unsigned int seconds, bool isBLEAdvisor, bool isLora);
   private:
        static unsigned int getUptime();
};

#endif
