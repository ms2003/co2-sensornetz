/*
 * SensorCom.cpp - Library for managinf autgentication and transmission of
 * measurements to the THC-Server.
 *
 * Created by Martin Steinbach, December, 2020.
 *
 * See file LICENSE for license information.
 */

#include "SensorCom.h"
#include <esp32-hal-log.h>

WiFiClientSecure client;

static String readFromFlash(String key){
    return NVS.getString(key);
}

static char* s2c(String str) {
    if(str.length()!=0){
        char* c = const_cast<char*>(str.c_str());
        return c;
    }
}

static String normalizePEM(String oneline) {
    int len = oneline.length();
    int newlines = len / 64 + 3;
    String pem = "";

    int minusCount = 0;
    int k = 0; // break count
    int s = 0;
    for (int i = 0; i < len ; i++ ) {
        s++;
        if (oneline[i] == '-') {minusCount++;}
        if (minusCount == 10) {
            //log_v("FROM 0 TO %d", i);
            pem.concat(oneline.substring(0,i + 1) + '\n');
            minusCount++;
            s = 0;
        }
        if (s == 64) {
            //log_v("FROM %d TO %d", i-63, i);
            s = 0;
            pem.concat(oneline.substring(i-63,i + 1) + '\n');
        }
        if (minusCount == 12) {
            pem.concat(oneline.substring(i - s + 1 ,i));
            pem.concat('\n' + oneline.substring(i)  );
            break;
        }
    }
    return pem;
}

void SensorCom::setServer(){

    _url = NVS.getString(K_DATA_URL);

    //_server = "sensor.nachtsieb.de"; //TODO: LCBURL is buggy -> new config parameter
    _server = NVS.getString(K_HOSTNAME);
    _port = 443;

/*
    String sca = normalizePEM(readFromFlash(K_CACERT));
    String scert = normalizePEM(readFromFlash(K_CERT));
    String skey = normalizePEM(readFromFlash(K_KEY));
*/
    String sca = readFromFlash(K_CACERT);
    String scert = readFromFlash(K_CERT);
    String skey = readFromFlash(K_KEY);

    _cacert = sca;
    _cert = scert;
    _key = skey;
}

void SensorCom::sendPost(String payload) {

    if(payload == NULL || payload.length() <= 0){
        log_e("no payload");
        return;
    }

    if (!client.connect(
                _server.c_str(),
                _port, _cacert.c_str(),
                _cert.c_str(),
                _key.c_str()))
    {
        log_e("unable to connect to %s on %d", _server.c_str(), _port);
        return;
    }

    // build request
    String length = String(payload.length());

    client.println("POST " + _url + " HTTP/1.1");
    client.println("Host: " + _server);
    client.println("Content-Length: " + length);
    client.println("Content-Type: application/json");
    client.println("Connection: close");
    client.println();
    client.println(payload);
    client.println();

    if (client.connected()) {
        String rHeader = client.readStringUntil('\n');
        String rCode = rHeader.substring(9,13);
        String rDescr = rHeader.substring(13);
        log_d("reponse code: %s - Description: %s\n", rCode.c_str(), rDescr.c_str());
    }

    client.stop();
}
