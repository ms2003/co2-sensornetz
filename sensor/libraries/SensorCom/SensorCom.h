/*
 * SensorCom.h - Library for managinf autgentication and transmission of
 * measurements to the THC-Server.
 *
 * Created by Martin Steinbach, December, 2020.
 *
 * See file LICENSE for license information.
 */

#ifndef SensorCom_h
#define SensorCom_h
#include "Arduino.h"
#include "ArduinoNvs.h"
#include "SensorSetup.h"
#include <WebServer.h>
#include <WiFiClientSecure.h>

class SensorCom {
    public:
        void setServer();
        void sendPost(String payload);
   private:
        String _server;
        uint16_t _port;
        String _url;
        String _cacert;
        String _cert;
        String _key;
};

#endif
