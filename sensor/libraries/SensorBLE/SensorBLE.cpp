/*
 * SensorBLE.cpp - Library for sending BLE iBeacons.
 *
 * Created by Martin Steinbach, December, 2020.
 *
 * See file LICENSE for license information.
 */
#include "SensorBLE.h"

void SensorBLE::advertise(String uuid, String id, unsigned int time) {

    BLEAdvertising *pAdvertising;

    // initiate advertising
    BLEDevice::init(id.c_str());
    pAdvertising = BLEDevice::getAdvertising();
    BLEDevice::startAdvertising();

    // create a beacon
    BLEBeacon oBeacon = BLEBeacon();
    oBeacon.setManufacturerId(0x4C00); // fake Apple 0x004C LSB (ENDIAN_CHANGE_U16!)
    oBeacon.setProximityUUID(BLEUUID(uuid.c_str()));
    oBeacon.setMajor(1);
    oBeacon.setMinor(22);

    BLEAdvertisementData oAdvertisementData = BLEAdvertisementData();

    oAdvertisementData.setFlags(0x04); // BR_EDR_NOT_SUPPORTED 0x04

    std::string strServiceData = "";

    strServiceData += (char)26;     // Len
    strServiceData += (char)0xFF;   // Type
    strServiceData += oBeacon.getData();

    oAdvertisementData.addData(strServiceData);

    pAdvertising->setAdvertisementData(oAdvertisementData);
    pAdvertising->setAdvertisementType(ADV_TYPE_NONCONN_IND);

    pAdvertising->start();

    // advertise for time millis
    if (time != 0) {
        delay(time);
        pAdvertising->stop();

        // free ram
        delete pAdvertising;
        BLEDevice::stopAdvertising();
        BLEDevice::deinit(true);
    }

}
