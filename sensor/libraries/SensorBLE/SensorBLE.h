/*
 * SensorBLE.h - Library for sending BLE iBeacons.
 *
 * Created by Martin Steinbach, December, 2020.
 *
 * See file LICENSE for license information.
 */

#ifndef SensorBLE_h
#define SensorBLE_h
#include "Arduino.h"
#include "SensorSetup.h"
#include "BLEDevice.h"
#include "BLEUtils.h"
#include "BLEBeacon.h"

class SensorBLE {
    public:
        static void advertise(String uuid, String id, unsigned int time);
};

#endif
